# Tools for working with MeteoSwiss' Climap and INCA data
by Jonas Wied Pedersen, PhD student at the Technical University of Denmark <br />
Developed in the period: October 7th, 2018 - January 31st, 2019

## Climap
- **"rain_gauge_locations.dat"** <br /> 
Downloaded file from MeteoSwiss' remote connection. It contains one time step of precipitation from all Climap stations. The purpose of this is to get the station information in the header of the file.

- **"rain_gauge_locations.R"** <br />
R-script that reads "rain_gauge_locations.dat" and produces a table with information about all Climap stations ("rain_gauge_info_all_climap_stations.Rdata").

- **"rain_gauge_info_all_climap_stations.Rdata"** <br /> 
An Rdata-file ready for reading into R with all Climap station information. Variables are "station number", "station_name", "elevation_meters", "x_coord_CH1903", ""

-----------------------------------------------------

- **"raingauge_bas_20140101-20151231.dat"** & **"raingauge_bas_20160101-20181115.dat"** <br /> 
Downloaded files from MeteoSwiss' remote connection. They contain time series data from the Basel/Binningen gauge.

- **"read_process_meteoswiss_raingaugedata.R"** <br /> 
R-script that reads the two Basel gauge data files and produces a single continuous time series. 

- **"identify_rg_events.R"** <br /> 
R-script that takes a continuous time series of rainfall intensity and identifies individual rain events. It contains one user-defined parameter: the time required between to rainfall measurement before they are considered separate events.


## INCA
- **"INCA_coordinates_for_gis.R"** <br /> 
Generates a grid that is the same size as the INCA forecast domain (Switzerland + a buffer zone), where the xy-coordinates of each INCA grid cell is specified.
It also show how to find the grid cell that a rain gauge is located in (examples with Basel, Kloten, and Fluntern gauges).
This script also produces the "rain_gauge_information.Rdata"-file.

- **"rain_gauge_information.Rdata"** <br /> 
Data that contains information about the three Climap stations in Basel, Kloten and Fluntern.

- **"inca_neighborhood_extraction_functions.R"** <br /> 
R-script with several functions that can be used to read the INCA-asc files and extract features (e.g. variance in neighborhood, fraction of cells in neighborhood that contain rainfall).

- **"unzip_read_inca_forecast"** <br /> 
R-script that runs through the archive of INCA forecasts and extracts the 20 km square neighborhoods around the Basel and Kloten Climap stations.

## Unzipped forecasts at Eawag's Q-drive
On Eawag's Q-drive, under "JonasPedersen" -> "INCA_point_forecast" <br /> 
there is a folder called "Basel". This contains a 41x41 cell grid of INCA forecast centered on the Basel Climap station.
The data is already unzipped and ready to be loaded into R.



